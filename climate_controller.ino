#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

// ATmega328P PWM works on pins: 3, 5, 6, 9, 10, and 11
#define PIN_DHT_DATA 2
#define PIN_HEATER_ONOFF 3
#define PIN_HUMIDIFIER_ONOFF 4
#define PIN_FAN_PWM 5
#define PIN_LIGHT_PWM 6

#define DHT_TYPE DHT22 // DHT 22 (AM2302)

DHT_Unified dht(PIN_DHT_DATA, DHT_TYPE);
uint32_t dhtDelayMS;

float minTemp = 24;
float maxTemp = 36;

float minHumid = 60;
float maxHumid = 70;

float oldTemp = -12345;
float oldHumid = -12345;

void setup() {
  Serial.begin(9600);
  Serial.println(F("init"));

  pinMode(PIN_HEATER_ONOFF, OUTPUT);
  pinMode(PIN_HUMIDIFIER_ONOFF, OUTPUT);
  pinMode(PIN_FAN_PWM, OUTPUT);
  pinMode(PIN_LIGHT_PWM, OUTPUT);

  setOutputDevice(PIN_HEATER_ONOFF, "heater", LOW);
  setOutputDevice(PIN_HUMIDIFIER_ONOFF, "humidifier", LOW);

  printInfo();
  
  // init dht22
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  dht.humidity().getSensor(&sensor);
  dhtDelayMS = sensor.min_delay / 1000;
  delay(dhtDelayMS);

  Serial.println(F("ready"));
}

void setOutputDevice(int pin, String name, int newstate) {
  Serial.print(F("! Turning "));
  Serial.print(name);
  if (newstate == HIGH) Serial.println(F(" ON"));
  if (newstate == LOW) Serial.println(F(" OFF"));

  if (digitalRead(pin) != newstate)
    digitalWrite(pin, newstate);
  else {
    Serial.print(F("! Refusing to make the change, already "));
    if (newstate == HIGH) Serial.println(F("ON"));
    if (newstate == LOW) Serial.println(F("OFF"));
  }
  Serial.println(F(""));
}

void printInfo() {
  Serial.println("Printing information:");
  Serial.print(F("minTemp: "));
  Serial.println(minTemp);
  Serial.print(F("maxTemp: "));
  Serial.println(maxTemp);
  
  Serial.print(F("minHumid: "));
  Serial.println(minHumid);
  Serial.print(F("maxHumid: "));
  Serial.println(maxHumid);
  Serial.println();
}

bool checkAndExtractFloat(String input, String cmd, float *out) {
  if (input.length() < cmd.length()+3) return false;
  if (!input.startsWith(cmd)) return false;
  
  *out = input.substring(cmd.length()+1).toFloat();
  return true;
}

void loop() {
  sensors_event_t event;
  
  float temp;
  float humid;

  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading DHT22 temperature!"));
    temp = oldTemp;
  } else {
    temp = event.temperature;
  }

  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println(F("Error reading DHT22 humidity!"));
    humid = oldHumid;
  } else {
    humid = event.relative_humidity;
  }

  // BEGIN LOGIC

  //Serial.print(F("Temperature: "));
  //Serial.print(temp);
  //Serial.println(F("°C"));

  //Serial.print(F("Humidity: "));
  //Serial.print(humid);
  //Serial.println(F("%"));

  if (temp != oldTemp) {
    Serial.print(F("Temperature changed from "));
    Serial.print(oldTemp);
    Serial.print(F(" to "));
    Serial.println(temp);

    if (temp < minTemp) {
      Serial.println(F("Turning heater ON"));
      digitalWrite(PIN_HEATER_ONOFF, HIGH);
    } else if (temp > maxTemp) {
      Serial.println(F("Turning heater OFF"));
      digitalWrite(PIN_HEATER_ONOFF, LOW);
    }
  }

  if (humid != oldHumid) {
    Serial.print(F("Humidity changed from "));
    Serial.print(oldHumid);
    Serial.print(F(" to "));
    Serial.println(humid);
  
    if (humid < minHumid) {
      setOutputDevice(PIN_HUMIDIFIER_ONOFF, "humidifier", LOW);
    } else if (humid > maxHumid) {
      setOutputDevice(PIN_HUMIDIFIER_ONOFF, "humidifier", HIGH);
    }
  }

  while (Serial.available() > 0) {
    String input = Serial.readStringUntil('\n');
    //if (Serial.available() > 0) Serial.read();

    float arg;
    if (input == "INFO") {
      printInfo();
    }
    else if (checkAndExtractFloat(input, "SETMINTEMP", &arg)) {
      Serial.print(F("! Set minimum temperature to: "));
      Serial.println(arg);
      minTemp = arg;
    }
    else if (checkAndExtractFloat(input, "SETMAXTEMP", &arg)) {
      Serial.print(F("! Set maximum temperature to: "));
      Serial.println(arg);
      maxTemp = arg;
    }
    else if (checkAndExtractFloat(input, "SETMINHUMID", &arg)) {
      Serial.print(F("! Set minimum humidity to: "));
      Serial.println(arg);
      minHumid = arg;
    }
    else if (checkAndExtractFloat(input, "SETMAXHUMID", &arg)) {
      Serial.print(F("! Set maximum humidity to: "));
      Serial.println(arg);
      maxHumid = arg;
    }
    else if (checkAndExtractFloat(input, "SETFANPWM", &arg)) {
      Serial.print(F("! Set fan pwm to: "));
      Serial.println(arg);
      analogWrite(PIN_FAN_PWM, (int)arg);
    }
    else if (checkAndExtractFloat(input, "SETLIGHTPWM", &arg)) {
      Serial.print(F("! Set light pwm to: "));
      Serial.println(arg);
      analogWrite(PIN_LIGHT_PWM, (int)arg);
    }

  }

  // END LOGIC

  oldTemp = temp;
  oldHumid = humid;
  
  delay(dhtDelayMS);
}
